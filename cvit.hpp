/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Max Ehrlich
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <opencv2/opencv.hpp>
#include <iterator>
#include <functional>
#include <stdexcept>
#include <type_traits>

namespace cvit
{

template<class AttrType, class MatType, class CRTP, bool is_const_iterator>
class attr_iterator : public std::iterator<std::forward_iterator_tag, MatType>
{
public:
    typedef typename std::conditional<is_const_iterator, const MatType, MatType>::type MatMutType;

    attr_iterator(void) = default;

    attr_iterator(const attr_iterator &it) = default;

    attr_iterator(cv::Mat mat, AttrType loc = 0)
        : mat(mat), pos(loc), endGuard(false)
    { }

    CRTP &operator++(void)
    {
        increment_attribute(pos);
        return static_cast<CRTP &>(*this);
    }

    CRTP operator++(int)
    {
        CRTP tmp = static_cast<CRTP &>(*this);
        increment_attribute(pos);
        return tmp;
    }

    bool operator==(const attr_iterator &other)
    {
        if(other.endGuard)
        {
            return pos >= get_termination_condition(mat);
        }
        else
        {
            if(mat.data != other.mat.data)
                throw std::runtime_error("Iterator matrices are different");

            return other.pos == pos;
        }
    }

    bool operator!=(const attr_iterator &other)
    {
        return !(*this == other);
    }

    MatMutType operator*(void)
    {
        return get_submat(mat, pos);
    }

protected:
    void increment_operator_impl(void)
    {
        increment_attribute(pos);
    }

    virtual void increment_attribute(AttrType &) = 0;

    virtual MatType get_submat(cv::Mat, AttrType) = 0;

    virtual AttrType get_termination_condition(cv::Mat) = 0;

private:
    cv::Mat mat;
    AttrType pos;
    bool endGuard = true;
};

template<class MatType, class CRTP, bool is_const_iterator>
class rc_iterator_base : public attr_iterator<int, MatType, CRTP, is_const_iterator>
{
public:
    using attr_iterator<int, MatType, CRTP, is_const_iterator>::attr_iterator;

    rc_iterator_base(void) = default;

    rc_iterator_base(cv::Mat mat, int loc, int step)
        : attr_iterator<int, MatType, CRTP, is_const_iterator>(mat, loc), step(step)
    { }

protected:
    virtual void increment_attribute(int &p) { p += step; }

    virtual MatType get_submat(cv::Mat m, int c)  = 0;

    virtual int get_termination_condition(cv::Mat m) = 0;

private:
    int step = 1;
};

template<class MatType, class CRTP, bool is_const_iterator>
class column_iterator_base : public rc_iterator_base<MatType, CRTP, is_const_iterator>
{
public:
    using rc_iterator_base<MatType, CRTP, is_const_iterator>::rc_iterator_base;

protected:
    virtual MatType get_submat(cv::Mat m, int c)  = 0;

    virtual int get_termination_condition(cv::Mat m) { return m.cols; }
};

template<class MatType, class CRTP, bool is_const_iterator>
class row_iterator_base : public rc_iterator_base<MatType, CRTP, is_const_iterator>
{
public:
    using rc_iterator_base<MatType, CRTP, is_const_iterator>::rc_iterator_base;

protected:
    virtual MatType get_submat(cv::Mat m, int c) = 0;

    virtual int get_termination_condition(cv::Mat m) { return m.rows; }
};

template<bool is_const_iterator>
class column_mat_iterator_base : public column_iterator_base<cv::Mat, column_mat_iterator_base<is_const_iterator>, is_const_iterator>
{
public:
    using column_iterator_base<cv::Mat, column_mat_iterator_base<is_const_iterator>, is_const_iterator>::column_iterator_base;

protected:
    virtual cv::Mat get_submat(cv::Mat m, int c) { return m.col(c); }
};

template<bool is_const_iterator>
class row_mat_iterator_base : public row_iterator_base<cv::Mat, row_mat_iterator_base<is_const_iterator>, is_const_iterator>
{
public:
    using row_iterator_base<cv::Mat, row_mat_iterator_base<is_const_iterator>, is_const_iterator>::row_iterator_base;

protected:
    virtual cv::Mat get_submat(cv::Mat m, int c) { return m.row(c); }
};

template<bool is_const_iterator>
class column_expr_iterator_base : public column_iterator_base<cv::MatExpr, column_expr_iterator_base<is_const_iterator>, is_const_iterator>
{
public:
    using column_iterator_base<cv::MatExpr, column_expr_iterator_base<is_const_iterator>, is_const_iterator>::column_iterator_base;

protected:
    virtual cv::MatExpr get_submat(cv::Mat m, int c) { return m.col(c) + 0; }
};

template<bool is_const_iterator>
class row_expr_iterator_base : public row_iterator_base<cv::MatExpr, row_expr_iterator_base<is_const_iterator>, is_const_iterator>
{
public:
    using row_iterator_base<cv::MatExpr, row_expr_iterator_base<is_const_iterator>, is_const_iterator>::row_iterator_base;

protected:
    virtual cv::MatExpr get_submat(cv::Mat m, int c) { return m.row(c) + 0; }
};

typedef column_mat_iterator_base<false> column_iterator;
typedef column_mat_iterator_base<true> const_column_iterator;
typedef column_expr_iterator_base<false> column_expr_iterator;
typedef column_expr_iterator_base<true> const_column_expr_iterator;

typedef row_mat_iterator_base<false> row_iterator;
typedef row_mat_iterator_base<true> const_row_iterator;
typedef row_expr_iterator_base<false> row_expr_iterator;
typedef row_expr_iterator_base<true> const_row_expr_iterator;

}
