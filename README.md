OpenCV Utility Iterators
========================

Allow for iteration over rows and columns of an opencv matrix using the STL algorithm and numeric functions. 
Defines `cvit::column_iterator` for iterating over columns, `cvit::row_iterator` for iterating over rows, and the abstract
`attr_iterator` for defining new iterators based off of matrix attributes. 
This is a header-only library, simply include the header file in your search path to use.

Example
=======
```c++
    #include <cvit.hpp>
    
    // ...

    double m[4][4] = {{1.0, 2.0, 3.0, 4.0}, {5.0, 6.0 ,7.0, 8.0}, {9.0, 10.0, 11.0, 12.0}, {13.0, 14.0, 15.0, 16.0}};
    cv::Mat mat(4, 4, CV_64F, m);

    cv::Mat colSum = std::accumulate(cvit::column_iterator(mat), cvit::column_iterator(), cv::Mat::zeros(4, 1, CV_64F));
    cv::Mat rowSum = std::accumulate(cvit::row_iterator(mat), cvit::row_iterator(), cv::Mat::zeros(1, 4, CV_64F));

    std::cout << "Sum all rows/columns: " << std::endl;
    std::cout << colSum << std::endl;
    std::cout << rowSum << std::endl;
    std::cout << std::endl;

    colSum = std::accumulate(cvit::column_iterator(mat, 2), cvit::column_iterator(), cv::Mat::zeros(4, 1, CV_64F));
    rowSum = std::accumulate(cvit::row_iterator(mat, 2), cvit::row_iterator(), cv::Mat::zeros(1, 4, CV_64F));

    std::cout << "Sum every other row/column: " << std::endl;
    std::cout << colSum << std::endl;
    std::cout << rowSum << std::endl;
    std::cout << std::endl;

    colSum = std::accumulate(cvit::column_iterator(mat, 1, 1), cvit::column_iterator(mat, 1, 3), cv::Mat::zeros(4, 1, CV_64F));
    rowSum = std::accumulate(cvit::row_iterator(mat, 1, 1), cvit::row_iterator(mat, 1, 3), cv::Mat::zeros(1, 4, CV_64F));

    std::cout << "Sum only row/column 1 and 2: " << std::endl;
    std::cout << colSum << std::endl;
    std::cout << rowSum << std::endl;
    std::cout << std::endl;
    
    cv::Mat normedData = cv::Mat::zeros(4, 4, CV_64F);
    std::transform(cvit::column_iterator(mat), cvit::column_iterator(), cvit::column_iterator(normedData), [](cv::Mat col) { return col / cv::norm(col); });

    std::cout << "Normalize columns of \"data matrix\": " << std::endl;
    std::cout << normedData << std::endl;
    std::cout << std::endl;
```